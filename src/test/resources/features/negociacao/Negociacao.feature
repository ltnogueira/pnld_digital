#language: pt
#encoding: utf-8
#autor: Leonardo Nogueira
#Tarefa: xxxx
#Versão: 1.0

#Todas as funcionalidades do Menu Adesão


Funcionalidade: Negociacao

@AcessarNegociacao
Cenario: Fazer login com sucesso
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Negociação"
Entao sistema apresenta o seguinte texto "Negociação"


@CadastrarNegociacao
Cenario: Fazer login com sucesso
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Negociação"
E clico no botao "Cadastrar Negociação"
Entao sistema apresenta o seguinte texto "Negociação"