#language: pt

Funcionalidade: Fazer login

@FazerLogin
Cenario: Deve realizar login com sucesso
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"

Quando clico no botao entrar

Entao sistema apresenta o seguinte texto "Programa Nacional do Livro e Material Didático"
E encerra o navegador


@FazerLogin
Cenario: Não deve realizar login com dados inexistentes na base de dados
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "teste.teste+123@redspark.io" 
E preencho o campo Senha "Asdf!2345678"

Quando clico no botao entrar

Entao sistema apresenta o seguinte texto "Login"
E encerra o navegador