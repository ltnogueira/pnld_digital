#language: pt
#encoding: utf-8
#autor: Leonardo Nogueira
#Tarefa: xxxx
#Versão: 1.0

#Todas as funcionalidades do Menu Adesão


Funcionalidade: Editar Objeto

@AcessarAdesao
Cenario: Fazer login com sucesso
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E aciono o menu principal
E aciono o submenu "Adesão"
Entao sistema apresenta o seguinte texto "Habilitar Adesão"