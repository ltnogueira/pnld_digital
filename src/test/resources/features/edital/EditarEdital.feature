#language: pt

Funcionalidade: Editar edital

@EditarEdital
Cenario: Deve salvar um edital
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"

Quando aciono o opcao de editar
E altero o titulo do edital
E clico no botao Salvar

Entao encerra o navegador


@EditarEdital
Cenario: Deve validar campos obrigatorios
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"

Quando aciono o opcao de editar
E limpo os preenchimentos dos campos
E clico no botao Salvar

Entao sistema apresenta o seguinte texto "Campo obrigatório"
E encerra o navegador
