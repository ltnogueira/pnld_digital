#language: pt

Funcionalidade: Cadastrar edital


@CadastrarEdital
Cenario: Cadastrar edital
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"

Quando clico no botao Cadastrar edital 
E insiro o titulo do edital
E insiro o ano de referencia
E escolho o tipo do edital
E insiro o numero do edital
E clico no botao Salvar

Então encerra o navegador


@CadastrarEdital
Cenario: Cadastrar edital sem campos obrigatorios preenchidos
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"

Quando clico no botao Cadastrar edital 
E clico no botao Salvar

Então sistema apresenta o seguinte texto "Campo obrigatório"
E encerra o navegador