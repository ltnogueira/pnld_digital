#language: pt

Funcionalidade: Pesquisar editora

@PesquisarEditora
Cenario: Pesquisar editora não existente
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"
E clico no dropdown da coluna Objeto para expandir
E clico na acao Ver mais da sublista

Quando clico na opcao Editar
E clico na aba Editoras
E clico no campo de Pesquisa
E digito o nome da editora
E clico no botao lupa

Então o sistema realiza a pesquisa
E apresenta mensagem Não foram encontrados resultados referentes a busca testes. Pesquise novamente.
E apresenta mensagem Nenhum registro encontrado.
E encerra o navegador