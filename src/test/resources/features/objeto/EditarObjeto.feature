#language: pt

Funcionalidade: Editar objeto

@EditarObjeto
Cenario: Editar objeto
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"
E clico no dropdown da coluna Objeto para expandir
E clico na acao Ver mais da sublista

Quando clico na opcao Editar
E preencho o campo Nome com Nome "Objeto Teste do Henrique ALTERADO"
E preencho o campo Codigo com codigo "12"
E preencho o campo Limite de Obras com limte de obras "123"
E preencho o campo Data e hora de abertura com "25/04/2023 00:00"
E preencho o campo Data e hora de fechamento com "25/04/2025 00:00"
E clico no botao Atualizar

Entao encerra o navegador
