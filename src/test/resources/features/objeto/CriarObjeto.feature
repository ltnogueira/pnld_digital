#language: pt

Funcionalidade: Criar objeto

@CriarObjeto
Cenario: Deve validar campos obrigatorios
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"
E clico na acao Ver mais

Quando clico na opcao Criar objeto
E clico no botao Inserir

Então sistema apresenta o seguinte texto "Campo obrigatório"
E encerra o navegador


@CriarObjeto
Cenario: Deve criar objeto
Dado que acesso o sistema pnld digital
E preencho o campo Usuario "andre.dellatorre+83464047865@redspark.io" 
E preencho o campo Senha "Asdf!2345678"
E clico no botao entrar
E aciono o menu principal
E aciono o submenu "Edital"
E clico na acao Ver mais

Quando clico na opcao Criar objeto
E preencho o campo Nome com Nome "Objeto Teste do Henrique"
E preencho o campo Codigo com codigo "66"
E preencho o campo Limite de Obras com limte de obras "666"
E preencho o campo Data e hora de abertura com "25/04/2023 00:00"
E preencho o campo Data e hora de fechamento com "25/04/2024 00:00"
E clico no botao Inserir

Então encerra o navegador