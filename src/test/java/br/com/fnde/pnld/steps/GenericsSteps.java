package br.com.fnde.pnld.steps;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

public class GenericsSteps extends CustomUtils {

	PaginaMainPage paginaMainPage = new PaginaMainPage();
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	@Test
	@Dado("que acesso o sistema pnld digital")
	public void que_acesso_o_sistema_pnld_digital() {
		String pnldUrl = "https://adesao.digi.tst.apps.rnp.br/login";
		CustomUtils.abrirNavegador(pnldUrl);
	}
	@Test
	@E("^preencho o campo Usuario \"([^\"]*)\"$")
	public void preenchoOCampoUsuario(String usuario) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.escreverUsuario(usuario);
	}
	
	@Test
	@E("^preencho o campo Senha \"([^\"]*)\"$")
	public void preenchoOCampoSenha(String senha) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.escreverSenha(senha);
	}

	@Test
	@E("clico no botao entrar")
	public void clico_no_botao_entrar() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		capturarTela("Screenshors");
		paginaMainPage.botaoEntrar();
	}
	@Test
	@Entao("^sistema apresenta o seguinte texto \"([^\"]*)\"$")
	public void sistemaApresentaOSeguinteTexto(String arg1) throws Exception {
		Thread.sleep(3000);
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.validarDetalhePagina(arg1);
		capturarTela(arg1);
    }
	@Test
	@E("^aciono o menu principal$")
	public void acionoOMenuPrincipal() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		Thread.sleep(5000);
	    paginaMainPage.clicarMenuPrincipal();
	    Thread.sleep(5000);
		capturarTela("Screenshorts");
	}
	@Test
	@E("^aciono o submenu \"([^\"]*)\"$")
	public void acionoOSubmenu(String submenu) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		Thread.sleep(2000);
		paginaMainPage.escolherSubmenu(submenu);
		Thread.sleep(2000);
		capturarTela("Screenshorts");
	}
	
	@Test
	@E("^clico no dropdown da coluna Objeto para expandir")
	public void clico_no_dropdown_da_coluna_Objeto_para_expandir() throws Exception {	
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.clicarDropExpandir();
	}
	
	@Test
	@E("clico na acao Ver mais da sublista")
	public void clico_na_acao_Ver_mais_da_sublista() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.clicarAcoesVermaisDoObjeto();
	}
	
	@Test
	@E("clico na opcao Editar")
	public void clico_na_opcao_Editar() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.clicarMenuEditar();
	}
	
    @E("encerra o navegador")
    public void encerra_o_navegador() {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.fechaBrowser();
        
    }
    
    @Test
	@E("^preencho o campo Nome com Nome \"([^\"]*)\"$")
	public void preencho_o_campo_Nome_com_nome(String nome) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.escreverNomeDoObjeto(nome);
	}

	@Test
	@E("^preencho o campo Codigo com codigo \"([^\"]*)\"$")
	public void preencho_o_campo_Codigo_com_codigo(String codigo) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.escreverCodigoDoObjeto(codigo);
	}


	@E("^clico no botao \"([^\"]*)\"$")
	public void clicoNoBotao(String arg1) throws  Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.clicarQualquerBotao(arg1);
	}
}
