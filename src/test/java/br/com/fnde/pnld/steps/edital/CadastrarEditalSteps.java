package br.com.fnde.pnld.steps.edital;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.E;

public class CadastrarEditalSteps  extends CustomUtils {
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	PaginaMainPage paginaMainPage = new PaginaMainPage();
	
    @Test
	@E("^aciono o submenbu Edital$")
	public void acionoOSubmenbuEdital() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.subMenus();
		Thread.sleep(3000);
		capturarTela("EditarEdital");
	}

    @Test
	@E("^clico no botao Cadastrar edital$")
	public void clico_no_botao_Cadastrar_edital () throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.cadastrarEdital();

    }
    
    @Test
	@E("^insiro o titulo do edital$")
	public void insiro_o_titulo_do_edital () throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.inserirTituloDoEdital();

    }
    
    @Test
	@E("^insiro o ano de referencia$")
	public void insiro_o_ano_de_referencia () throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.inserirAnoDeReferencia();

    }
    
    @Test
	@E("^escolho o tipo do edital$")
	public void escolho_o_tipo_do_edital () throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.escolherTipoDoEdital();

    }
    
    @Test
	@E("^insiro o numero do edital$")
	public void einsiro_o_numero_do_edital () throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.inserirNumeroDoEdital();

    }

    @Test
    @E("sistema apresenta botao Cancelar")
    public void sistema_apresenta_botao_cancelar() {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.temBotaoCancelar();
    
    }
    
    @Test
    @E("sistema apresenta botao Salvar")
    public void sistema_apresenta_botao_salvar() {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.temBotaoSalvar();
    }
    
    @Test
    @E("^preencho o campo Limite de Obras com limte de obras \"([^\"]*)\"$")
    public void preencho_o_campo_Limite_de_Obras_com_limte_de_obras (String limiteDeObras) throws Exception {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.escreverLimiteDeObras(limiteDeObras);
    }
    
    @Test
    @E("^preencho o campo Data e hora de abertura com \"([^\"]*)\"$")
    public void preencho_o_campo_Data_e_hora_de_abertura_com (String dataHoraDeAbertura) throws Exception {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.escreverDataHoraDeAbertura(dataHoraDeAbertura);
    }
    
    
    @Test
    @E("^preencho o campo Data e hora de fechamento com \"([^\"]*)\"$")
    public void preencho_o_campo_Data_e_hora_de_fechamento_com (String dataHoraDeFechamento) throws Exception {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.escreverDataHoraDeFechamento(dataHoraDeFechamento);
    }
    
    
    @Test
    @E("clico no botao Inserir")
    public void clico_no_botao_Inserir () throws Exception {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.clicarBotaoInserir();
        
    }
}
