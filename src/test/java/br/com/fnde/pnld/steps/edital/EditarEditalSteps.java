package br.com.fnde.pnld.steps.edital;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.E;

public class EditarEditalSteps  extends CustomUtils {
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	PaginaMainPage paginaMainPage = new PaginaMainPage();
	
	@Test
	@E("^aciono o opcao de editar$")
	public void acionoOOpcaoDeEditar() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.opcaoEditar();
		Thread.sleep(3000);
		capturarTela("EditarEdital");
}
	
	@Test
    @E("limpo os preenchimentos dos campos")
    public void limpo_os_preenchimentos_dos_campos() throws Exception{
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.limparCampos();
    }
	
    @Test
    @E("clico no botao Salvar")
    public void clico_no_botao_Salvar() throws Exception {
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.cilicarBotaoSalvar();
    }
   
    @Test
    @E("altero o titulo do edital")
    public void  altero_o_titulo_do_edital() throws Exception{
        PaginaMainPage paginaMainPage = new PaginaMainPage();
        paginaMainPage.alterarInformacaoEdital();
    }
}