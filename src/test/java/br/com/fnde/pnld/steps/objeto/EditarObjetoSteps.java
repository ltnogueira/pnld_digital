package br.com.fnde.pnld.steps.objeto;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.E;

public class EditarObjetoSteps extends CustomUtils {
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	PaginaMainPage paginaMainPage = new PaginaMainPage();

	@Test
	@E("clico no botao Atualizar")
	public void clico_no_botao_Atualizar() throws Exception {
		
		paginaMainPage.clicarBotaoAtualizar();
	}

	
}

	