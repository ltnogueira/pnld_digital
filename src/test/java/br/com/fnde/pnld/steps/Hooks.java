package br.com.fnde.pnld.steps;

import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks extends CustomUtils{


	@Before
	public void beforeScenario(Scenario scenario){}
	void capturarTela() {
		abrirNavegador(null);
	}
	
	
	@After
	public void afterScenario(){
		CustomUtils.getDriver();
			CustomUtils.getDriver().quit();
		}
}
