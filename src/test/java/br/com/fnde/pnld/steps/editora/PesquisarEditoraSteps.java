package br.com.fnde.pnld.steps.editora;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.E;

public class PesquisarEditoraSteps extends CustomUtils {
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	PaginaMainPage paginaMainPage = new PaginaMainPage();

	@Test
	@E("clico na aba Editoras")
	public void clico_na_aba_editoras() throws Exception {
	    paginaMainPage.clicarAbaEditoras();
	}

	@Test
	@E("clico no campo de Pesquisa")
	public void clico_no_campo_de_pesquisa() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@Test
	@E("digito o nome da editora")
	public void digito_o_nome_da_editora() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@Test
	@E("clico no botao lupa")
	public void clico_no_botao_lupa() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@Test
	@E("o sistema realiza a pesquisa")
	public void o_sistema_realiza_a_pesquisa() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@Test
	@E("apresenta mensagem Não foram encontrados resultados referentes a busca testes. Pesquise novamente.")
	public void apresenta_mensagem_não_foram_encontrados_resultados_referentes_a_busca_testes_pesquise_novamente() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@Test
	@E("apresenta mensagem Nenhum registro encontrado.")
	public void apresenta_mensagem_nenhum_registro_encontrado() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}
}	