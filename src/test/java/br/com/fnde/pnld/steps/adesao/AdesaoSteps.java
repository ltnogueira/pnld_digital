package br.com.fnde.pnld.steps.adesao;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import br.com.fnde.pnld.util.CustomUtils;

public class AdesaoSteps  extends CustomUtils {

private static WebDriver driver;


public static void capturarTelaAdesao(String fileName) throws Exception {

	// Captura a imagem da tela e armazena em um objeto do tipo File
	File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	// Salva o arquivo em um local específico
	Date data = new Date();
	try {
		FileUtils.copyFile(screenshot, new File("C:\\Users\\VERTIGO\\git\\pnld_digital\\evidencias\\Adesao"+fileName+ data.getTime()+".jpeg"),true);
	} catch (IOException e) {
		e.printStackTrace();
	}
}
}

