package br.com.fnde.pnld.steps.objeto;

import org.junit.Test;

import br.com.fnde.pnld.pages.PaginaMainPage;
import br.com.fnde.pnld.pages.PaginaMainPageElementMap;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.java.pt.E;

public class CriarObjetoSteps extends CustomUtils {
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	PaginaMainPage paginaMainPage = new PaginaMainPage();
	
	
	
	@Test
	@E("clico na acao Ver mais")
	public void clico_na_acao_Ver_mais() throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.clicarAcaoVerMais();
	
	}

	@Test
	@E("clico na opcao Criar objeto")
	public void clico_na_opcao_Criar_objeto() throws Exception {
		paginaMainPage.criarObjeto();

	}

}
