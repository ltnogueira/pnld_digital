package br.com.fnde.pnld.util;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CustomUtils extends TestWatcher {

	private static WebDriver driver;

	public CustomUtils(){
		super();
	}
	
	@Override
	protected void starting(Description description){}

	protected void finished(Description description){
		super.finished(description);
	}

	public static WebDriver getDriver(){
		return driver;
	}

	public static void abrirNavegador(String url){
		ChromeOptions chromeOpts = new ChromeOptions();
		chromeOpts.addArguments("start-maximized");
		chromeOpts.addArguments("disable-dev-shm-usage");
		chromeOpts.addArguments("ignore-ssl-errors=yes");
		chromeOpts.addArguments("ignore-certificate-errors");
		chromeOpts.addArguments("--remote-allow-origins=*");
		Utils.setDriverByOS();

		driver = new ChromeDriver(chromeOpts);
		driver.manage().window().maximize();
		driver.navigate().to(url);
	}


	public static void paginaContemTexto(String arg1) throws Exception {
		boolean existe = false;
		for (int i = 0; i < 4; i++) {
			existe = getDriver().getPageSource().contains(arg1);
			if (existe == false) {
				Thread.sleep(1000);
			} else {
				break;
			}
		}
		assertTrue(existe);

	}
	
	/**
	 * Método para capturar e salvar uma evidência
	 * @param fileName - Nome do arquivo
	 */
	public static void capturarTela(String fileName) throws Exception {

		// Captura a imagem da tela e armazena em um objeto do tipo File
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Salva o arquivo em um local específico
		Date data = new Date();
		try {
			FileUtils.copyFile(screenshot, new File("C:\\projetos\\"+fileName+ data.getTime()+".jpeg"),true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

		
}

