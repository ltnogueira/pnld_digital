package br.com.fnde.pnld.runners.adesao;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(	
		    plugin= {"pretty",
            "html:target/SystemTestReports/report.html",
            "json:target/SystemTestReports/report.json",
            "junit:target/SystemTestReports/junit.xml"},
			features = "classpath:features/Adesao.feature",
			tags = "@AcessarAdesao",
			glue = {"br.com.fnde.pnld.steps"}
			)
	public class AcessarAdesaoRunner {

		@ClassRule
		public static CustomUtils customultis = new CustomUtils();

	}

