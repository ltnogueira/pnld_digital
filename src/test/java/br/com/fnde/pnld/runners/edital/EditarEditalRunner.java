package br.com.fnde.pnld.runners.edital;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(	
		    plugin= {"pretty",
            "html:target/SystemTestReports/report.html",
            "json:target/SystemTestReports/report.json",
            "junit:target/SystemTestReports/junit.xml"},
			features = "classpath:features/edital/EditarEdital.feature",
			tags = "@EditarEdital",
			glue = {"br.com.fnde.pnld.steps"}
			)
	public class EditarEditalRunner {

		@ClassRule
		public static CustomUtils customultis = new CustomUtils();

	}

