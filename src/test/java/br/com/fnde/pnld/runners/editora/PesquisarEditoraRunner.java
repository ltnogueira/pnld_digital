package br.com.fnde.pnld.runners.editora;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(	
		    plugin= {"pretty",
            "html:target/SystemTestReports/report.html",
            "json:target/SystemTestReports/report.json",
            "junit:target/SystemTestReports/junit.xml"},
			features = "classpath:features/editora/PesquisarEditora.feature",
			tags = "@PesquisarEditora",
			glue = {"br.com.fnde.pnld.steps"}
			)
	public class PesquisarEditoraRunner {

		@ClassRule
		public static CustomUtils customultis = new CustomUtils();

	}

