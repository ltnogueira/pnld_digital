package br.com.fnde.pnld.runners.autenticacao;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import br.com.fnde.pnld.util.CustomUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/cucumber.html"}, 
				 features = "classpath:features", 
				 tags = "@FazerLogin", 
				 glue = {"br.com.fnde.pnld.steps" })
public class AutenticacaoRunner {

	@ClassRule
	public static CustomUtils customultis = new CustomUtils();

}
