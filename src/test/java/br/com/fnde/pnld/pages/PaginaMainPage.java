package br.com.fnde.pnld.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import br.com.fnde.pnld.util.CustomUtils;

public class PaginaMainPage extends PaginaMainPageElementMap {

	public PaginaMainPage() {
		PageFactory.initElements(driver, this);
	}

	public void escreverUsuario(String usuario) {

		campoUsuario.sendKeys(usuario);

	}

	public void escreverSenha(String senha) {
		campoSenha.sendKeys(senha);
	}

	public void botaoEntrar() {
		btnEntrar.click();
	}

	public void subMenus() {
		subMenuEdital.click();

	}

	public void escolherSubmenu(String submenu) {
		switch (submenu) {
		case "Adesão":
			subMenuAdesao.click();
		case "Edital":
			subMenuEdital.click();
		}
	}

	public void opcaoEditar() {
		opcaoEditar.click();
	}

	public void temBotaoCancelar() {
		botaoCancelar.isDisplayed();
	}

	public void temBotaoSalvar() {
		botaoCancelar.isDisplayed();
	}

	public void inserirTituloDoEdital() {
		campoTituloEdital.sendKeys("Edital - Henrique - 24042023");
	}

	public void inserirAnoDeReferencia() {
		campoAnoDeReferencia.sendKeys("2077");
	}

	public void escolherTipoDoEdital() throws InterruptedException {
		// Select dropdown = new Select(driver.findElement(By.id("type")));
		// dropdown.selectByVisibleText("PNLD");
		campoTipoDoEdital.click();
		Thread.sleep(1000);
		valorCampoTipoDoEdital.click();

	}

	public void inserirNumeroDoEdital() {
		campoNumeroDoEdital.sendKeys("0001");
	}

	public void cilicarBotaoSalvar() throws Exception {
		botaoSalvar.click();
		Thread.sleep(5000);
	}

	public void alterarInformacaoEdital() throws Exception {
		Thread.sleep(500);
		limparCampos();
		Thread.sleep(500);
		campoTituloEdital.sendKeys(" Teste automatizado em operação");
		Thread.sleep(500);
		campoAnoDeReferencia.sendKeys("2025");
		Thread.sleep(500);
		campoNumeroDoEdital.sendKeys("7788");
		Thread.sleep(500);
	}

	public void limparCampos() throws Exception {
		campoTituloEdital.sendKeys(Keys.CONTROL + "a");
		campoTituloEdital.sendKeys(Keys.DELETE);

		campoAnoDeReferencia.sendKeys(Keys.CONTROL + "a");
		campoAnoDeReferencia.sendKeys(Keys.DELETE);

		campoNumeroDoEdital.sendKeys(Keys.CONTROL + "a");
		campoNumeroDoEdital.sendKeys(Keys.DELETE);

		Thread.sleep(1000);
	}

	public void cadastrarEdital() throws Exception {
		botaoCadastrarEdital.click();
		Thread.sleep(3000);
	}

	public void criarObjeto() throws Exception {
		Thread.sleep(500);
		opcaoCriarObjeto.click();
		Thread.sleep(5000);
	}

	public void escreverNomeDoObjeto(String nome) {
		campoNomeObjeto.sendKeys(Keys.CONTROL + "a");
		campoNomeObjeto.sendKeys(Keys.DELETE);
		campoNomeObjeto.sendKeys(nome);
	}

	public void escreverCodigoDoObjeto(String codigo) {
		campoCodigo.sendKeys(Keys.CONTROL + "a");
		campoCodigo.sendKeys(Keys.DELETE);
		campoCodigo.sendKeys(codigo);
	}

	public void escreverLimiteDeObras(String limiteDeObras) throws Exception {
		Thread.sleep(500);
		campoLimiteDeObras.sendKeys(Keys.CONTROL + "a");
		campoLimiteDeObras.sendKeys(Keys.DELETE);
		campoLimiteDeObras.sendKeys(limiteDeObras);
	}

	public void escreverDataHoraDeAbertura(String dataHoraDeAbertura) throws Exception {
		Thread.sleep(500);
		campoDataHoraDeAbertura.sendKeys(Keys.CONTROL + "a");
		campoDataHoraDeAbertura.sendKeys(Keys.DELETE);
		campoDataHoraDeAbertura.sendKeys(dataHoraDeAbertura);
	}

	public void escreverDataHoraDeFechamento(String escreverDataHoraDeFechamento) throws Exception {
		Thread.sleep(500);
		campoDataHoraDeFechamento.sendKeys(Keys.CONTROL + "a");
		campoDataHoraDeFechamento.sendKeys(Keys.DELETE);
		campoDataHoraDeFechamento.sendKeys(escreverDataHoraDeFechamento);
	}

	public void clicarBotaoInserir() throws Exception {
		if (botaoInserir.isDisplayed()) {
			botaoInserir.click();
			Thread.sleep(3000);
		} else {
			System.err.println("Não existe o botão em tela...");
		}
	}

	public void fechaBrowser() {
		driver.quit();
	}
	// public void fecharBrowser(String usuario, String senha){
	// campoUsuario.sendKeys(usuario);
	// campoSenha.sendKeys(senha);
	// btnEntrar.click();
	// }

	public void validarDetalhePagina(String arg1) throws Exception {
		CustomUtils.paginaContemTexto(arg1);
	}

	public void clicarMenuPrincipal() throws Exception {
		menuPrincipal.click();
	}

	public void clicarDropExpandir() throws Exception {
		Thread.sleep(500);
		dropObjeto.click();
		Thread.sleep(500);

	}

	public void clicarAcoesVermaisDoObjeto() {
		acaoEditar.click();

	}

	public void clicarMenuEditar() throws Exception {
		Thread.sleep(500);
		subMenuEditar.click();
		Thread.sleep(10000);
	}

	public void clicarAcaoVerMais() throws Exception {
		Thread.sleep(500);
		acaoVerMais.click();
		Thread.sleep(500);
	}

	public void clicarBotaoAtualizar() throws Exception {
		Thread.sleep(2000);
		botaoAtualizar.click();
		Thread.sleep(2000);
	}

	/*
	 * public boolean isPesquisaRetornouResultados(){ return
	 * !lblQtdResultadosEncontrados.getText().contains("0"); }
	 */

	public void clicarQualquerBotao(String arg1){
		String xpath = "//span[contains(.,'" + arg1 + "')]";
		clicarBotao = driver.findElement(By.xpath(xpath));
		clicarBotao.click();

	}

	public void clicarAbaEditoras() throws Exception {
		Thread.sleep(3000);
		abaEditoras.click();
		Thread.sleep(3000);
		
	}
}
