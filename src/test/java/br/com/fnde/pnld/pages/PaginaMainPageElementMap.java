package br.com.fnde.pnld.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaginaMainPageElementMap extends PaginaPage{

	@FindBy(xpath = "//span[contains(text(),'Entrar')]")
	public WebElement btnEntrar;

	@FindBy(id = "username")
	protected WebElement campoUsuario;

	@FindBy(id = "password")
	protected WebElement campoSenha;
	
	@FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/button")
	protected WebElement menuPrincipal;
	
	@FindBy(xpath = "//span[contains(text(),'Editais')]")
	protected WebElement subMenuEdital;
	
	@FindBy(xpath = "/html/body/div[1]/div/div[3]/div[2]/div[3]/table/tbody/tr[1]/td[6]/div/div/button")
	protected WebElement opcaoEditar;
	
	@FindBy(xpath = "//span[contains(text(),'Adesão')]")
	protected WebElement subMenuAdesao;
	
	@FindBy (xpath = "//*[@id=\"btn-cancel\"]/span[1]")
	protected WebElement botaoCancelar;
	
	@FindBy (xpath = "//*[@id=\"btn-register\"]/span[1]")
	protected WebElement botaoSalvar;
	
	@FindBy (id = "title")
	protected WebElement campoTituloEdital;
	
	@FindBy (id = "year")
	protected WebElement campoAnoDeReferencia;
	
	@FindBy (xpath = "//*[@id=\"type\"]")
	protected WebElement campoTipoDoEdital;
	
	@FindBy (xpath = "//*[@id=\"menu-type\"]/div[3]/ul/li[2]/div/span")
	protected WebElement valorCampoTipoDoEdital;
	
	@FindBy (id = "number")
	protected WebElement campoNumeroDoEdital;
	
	@FindBy (xpath = "//*[@id=\"root\"]/div/div[3]/div[2]/div[2]/div/div[2]/div/button")
	protected WebElement botaoCadastrarEdital;
	
	@FindBy (xpath = "//*[@id=\"root\"]/div/div[3]/div[2]/div[3]/table/tbody/tr[1]/td[6]/div/button/span[1]")
	protected WebElement acaoVerMais;
	
	@FindBy (xpath = "//*[@id=\"edital_menu_9052b880-3762-4416-b371-3dcdb3cdc310\"]/div[3]/ul/li[1]")
	protected WebElement opcaoCriarObjeto;
	
	@FindBy (id = "name")
	protected WebElement campoNomeObjeto;
	
	@FindBy (id = "number")
	protected WebElement campoCodigo;
	
	@FindBy (id = "itemsLimit")
	protected WebElement campoLimiteDeObras;
	
	/*
	 * Mapeamento dos elementos da tela xpto
	 * 
	 */
	
	@FindBy (id = "openingDate")
	protected WebElement campoDataHoraDeAbertura;
	
	@FindBy (id = "closeDate")
	protected WebElement campoDataHoraDeFechamento;
	
	@FindBy (xpath = "//*[@id=\"btnRegisterOrUpdateObjectDetails\"]/span[1]")
	protected WebElement botaoInserir;
	
	@FindBy (xpath = "//*[@id=\"root\"]/div/div[3]/div[2]/div[3]/table/tbody/tr[1]/td[5]/button")
					  
	protected WebElement dropObjeto;
	
	@FindBy (xpath = "//*[@id=\"reportMenu-9052b880-3762-4416-b371-3dcdb3cdc310-26\"]/div[3]/ul/li[1]")
	protected WebElement subMenuEditar;
	
	@FindBy (xpath = "//*[@id=\"root\"]/div/div[3]/div[2]/div[3]/table/tbody/tr[2]/td/div/div/div/div/table/tbody/tr/td[6]/button/span[1]")
	protected WebElement acaoEditar;
	
	@FindBy (xpath = "//*[@id=\"btnRegisterOrUpdateObjectDetails\"]/span[1]")
	protected WebElement botaoAtualizar;
	
	@FindBy(xpath = "//span[contains(.,'__param__')]")
	protected WebElement clicarBotao;
	
	@FindBy (xpath = "//*[@id=\"root\"]/div/div[3]/div[2]/div[2]/div/div[3]/div/button[2]/span[1]")
	protected WebElement abaEditoras;
	
	}


